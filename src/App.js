import './App.css';
import React from 'react';
import Axios from "axios";
import { useState } from "react";
import { useTable } from 'react-table';
import { COLUMNS } from './columns';

function Table( {columns, data} )
{
  const
  {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable( {columns, data} )

  return (
    <table {...getTableProps()}>
        <thead>
            { headerGroups.map((headerGroup) => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    { headerGroup.headers.map((column) => (
                        <th {...column.getHeaderProps()}> { column.render('Header') } </th>
                    ))}
                </tr>
            ))}
        </thead>
        <tbody {...getTableBodyProps()}>
            { rows.map((row) => 
                {
                    prepareRow(row);
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map((cell) =>
                                {
                                    return <td {...cell.getCellProps()}> { cell.render('Cell') } </td>
                                }
                            )}
                        </tr>
                    )
                })
            }
        </tbody>
    </table>
  )
}

function App()
{
  const [data, setData] = useState([]);
  const [capHit, setCapHit] = useState(0);

  const apiCall = Axios.create(
  {
    baseURL: `http://localhost:3001/api/players`
  });

  var capHitArray;

  const getPlayers = async (filterSelection) =>
  {
    try
    {
      let returnedData = await apiCall.get(`/${filterSelection}`).then(( { data } ) => data);
      setData(returnedData);

      capHitArray = returnedData.map((val) => {return val.contractDetailsSalaryCapHit});

      setCapHit(capHitArray.reduce((a,v) => a = a + v, 0));
    }catch(err)
    {
      console.log(err);
    }
  }
  
  return (
    <div className="App">
      <div className="filterDropdown">
        <label>Player Group: </label>
        <select name="playerGroup" id="playerGroupSelect" required onChange={ (event) => { getPlayers(event.target.value) }}>
          <option value="" selected disabled hidden>Filter by...</option>
          <optgroup label="CoC Teams">
            <option value="akabo23@hotmail.com">Beef Supreme</option>
            <option value="bchelfrich@gmail.com">FootBallzDeep</option>
            <option value="jeffreyrpatterson@gmail.com">Hoping for 5-8!</option>
            <option value="nickw0023@gmail.com">Jegan’s Sportsbook</option>
            <option value="Njdevs95@gmail.com">JJ Twatt</option>
            <option value="michaelvan1298@gmail.com">Legion of Boom</option>
            <option value="zo31315@yahoo.com">My Balls Your Mouth</option>
            <option value="drkwlf77@yahoo.com">NoLivesMatter</option>
            <option value="GANESHANDEVANTE@gmail.com">Ol Dirty Chinese Res</option>
            <option value="jasontparker@gmail.com">Option Paralysis</option>
            <option value="robert.miller.329@gmail.com">Thick Blood</option>
            <option value="rmillz03@gmail.com">Unncessary Roughness</option>
            <option value="freeAgents">Free Agents</option>
          </optgroup>
        </select>
      </div>
      <div className="resultsTable">
        <Table columns={COLUMNS} data={data} />
      </div>
      <div className="salaryCapTotal">
        <h1>Total Salary Cap Hit: ${capHit}</h1>
      </div>
    </div>
  );
}

export default App;