export const COLUMNS = 
[
    {
        Header: 'Player Name',
        accessor: 'nflPlayersName'
    },
    {
        Header: 'Position',
        accessor: 'nflPlayersPosition'
    },
    {
        Header: 'NFL Team',
        accessor: 'nflPlayersTeam'
    },
    {
        Header: 'CoC Team',
        accessor: 'teamDataTeamName'
    },
    {
        Header: 'Contract Type',
        accessor: 'contractDetailsContractType'
    },
    {
        Header: 'Contract Length',
        accessor: 'contractDetailsContractLength'
    },
    {
        Header: 'Contract Base Value',
        accessor: 'contractDetailsBaseContractValue'
    },
    {
        Header: 'Salary Cap Hit',
        accessor: 'contractDetailsSalaryCapHit'
    }
]